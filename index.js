const express = require('express')
const app = express()

const equipes = require('./equipes.json')
app.use(express.json());


app.listen(82,() =>{
    console.log('REST API via ExpressJs')
})
// Liste des equipes:
app.get('/equipes', (req,res) => {
    // res.send("Liste des Equipes")
    res.status(200).json(equipes)
})
// Affichage des equipes par id:
app.get('/equipes/:id',(req,res) =>{
    const id = parseInt(req.params.id)
    const equipe = equipes.find(equipe=> equipe.id === id)
    res.status(200).json(equipe)
})
// Ajouter infos des equipes 
app.post('/equipes',(req,res)=>{
    equipes.push(req.body)
    res.status(200).json(equipes)
})
// Modifier infos des equipes 
app.put('/equipes/:id',(req,res)=>{
const id=parseInt(req.params.id);
let equipe=equipes.find(equipe=>equipe.id===id);
equipe.name=req.body.name,
equipe.country=req.body.country,
res.status(200).json(equipe);
})
// Supprimer infos des equipes 
app.delete('/equipes/:id',(req,res)=>{
const id=parseInt(req.params.id)
let equipe=equipes.find(equipe=>equipe.id===id)
equipes.splice(equipes.indexOf(equipe),1)
res.status(200).json(equipes)
})
//  JOUEURS
const joueurs = require("./joueurs.json");
app.use(express.json());

// Récupérer tous les joueurs
app.get("/joueurs", (req, res) => {
  res.status(200).json(joueurs);
});
// Ajouter un nouveau joueur
app.post("/joueurs", (req, res) => {
  joueurs.push(req.body);
  res.status(200).json(joueurs);
});
// Récupérer un joueur par son ID
app.get("/joueurs/:id",(req,res) =>{
    const id = parseInt(req.params.id)
    const joueur = joueurs.find(joueur=>joueur.id === id)
    if (!joueur) return res.status(404).send("Joueur non trouvé");
    res.status(200).json(joueur)
})
// Modifier infos des joueurs 
app.put('/joueurs/:id',(req,res)=>{
    const id=parseInt(req.params.id);
    let joueur=joueurs.find(joueur=>joueur.id===id);
    joueur.idEquipe=req.body.idEquipe,
    joueur.nom=req.body.nom,
    joueur.numero=req.body.numero,
    joueur.poste=req.body.poste
    res.status(200).json(joueur);
})
// Supprimer infos des joueurs 
app.delete('/joueurs/:id',(req,res)=>{
const id=parseInt(req.params.id);
let joueur=joueurs.find(joueur=>joueur.id===id);
joueurs.splice(joueurs.indexOf(joueur),1);
res.status(200).json(joueurs);
})
// Développer la route permettant d’afficher les joueurs d’une équipe via son id(de l’équipe

app.get('/equipesJoueur/:id', (req, res) => {
    const equipeId = parseInt(req.params.id);
    const joueursEquipe = joueurs.filter(joueur => joueur.idEquipe === equipeId);
    
    if (joueursEquipe.length > 0) {
        res.status(200).json(joueursEquipe);
    } else {
        res.status(404).json({ message: "Aucun joueur trouvé pour cette équipe" });
    }
});
// Développer la route permettant d’afficher l’équipe d’un joueur donné via son id

app.get('/JoueursEquipe/:id', (req, res) => {
    const joueurId = parseInt(req.params.id);
    const joueur = joueurs.find(joueur => joueur.id === joueurId);

    if (joueur) {
        const equipeId = joueur.idEquipe;
        const equipe = equipes.find(equipe => equipe.id === equipeId);

        if (equipe) {
            res.status(200).json(equipe);
        } else {
            res.status(404).json({ message: "L'équipe du joueur n'a pas été trouvée" });
        }
    } else {
        res.status(404).json({ message: "Joueur non trouvé" });
    }
});
// -Développer la route permettant de chercher un jour a partir de son nom

app.get('/joueurs/search', (req, res) => {
    const searchName = req.query.nom; 

    if (!searchName) {
        return res.status(400).json({ message: "Veuillez fournir un nom de joueur à rechercher" });
    }

    const joueur = joueurs.find(joueur => joueur.nom.toLowerCase() === searchName.toLowerCase());

    if (joueur) {
        res.status(200).json(joueur);
    } else {
        res.status(404).json({ message: "Aucun joueur trouvé avec ce nom" });
    }
});